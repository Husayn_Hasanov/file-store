<?php


namespace common\models\constants;


use Yii;

class UserRole
{
    const ROLE_MODERATOR = 50;
    const ROLE_ADMIN = 100;
    const ROLE_USER = 1;

    public static function getString($role)
    {
        if ($role == self::ROLE_MODERATOR) return Yii::t('app', "Moderator");
        if ($role == self::ROLE_ADMIN) return Yii::t('app', "Admin");
        if ($role == self::ROLE_USER) return Yii::t('app', "User");
        return \Yii::t('app', "Unknown");
    }

    public static function getList()
    {
        $roles = [
            self::ROLE_USER => self::getString(self::ROLE_USER),
            self::ROLE_MODERATOR => self::getString(self::ROLE_MODERATOR),
        ];

        if (\Yii::$app->user->identity->is_admin)
            $roles[self::ROLE_ADMIN] = self::getString(self::ROLE_ADMIN);
        return $roles;
    }
}
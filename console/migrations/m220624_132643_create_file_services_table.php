<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%file_services}}`.
 */
class m220624_132643_create_file_services_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%file_services}}', [
            'id' => $this->primaryKey(),
            'file' => $this->string(),
            'user_id' => $this->integer()->notNull(),
            'hash' => $this->string(),
            'original_name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%file_services}}');
    }
}

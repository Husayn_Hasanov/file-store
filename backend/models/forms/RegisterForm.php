<?php


namespace backend\models\forms;

use common\models\constants\GeneralStatus;
use common\models\constants\UserRole;
use common\models\User;
use Yii;
use yii\base\Model;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $full_name
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property int $role
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string $password write-only password
 * @property string $confirm_password
 * @property bool $change_password
 *
 * @property User $identity
 */
class RegisterForm extends Model
{
    public $id;
    public $username;
    public $full_name;
    public $role;
    public $status;
    public $password;
    public $confirm_password;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['username', 'full_name', 'password', 'confirm_password', 'role'], 'required'],
            [['status', 'change_password', 'role'], 'integer'],
            ['status', 'default', 'value' => GeneralStatus::STATUS_INACTIVE],
            ['role', 'default', 'value' => UserRole::ROLE_USER],
            ['status', 'default', 'value' => GeneralStatus::STATUS_ACTIVE],
            [['username', 'full_name'], 'string', 'max' => 255],
            [['username'], 'match', 'pattern' => '/^[a-z]\w*$/i', 'message' => '5 tadan kam bo\'lmagan, faqat lotin kichik harflari bo\'lishi kerak'],
            ['confirm_password', 'compare', 'compareAttribute' => 'password', 'message' => 'Parollar bir-biriga mos kelmaydi'],
            ['username', 'unique', 'targetClass' => User::className(),
                'message' => 'Ushbu login oldindan mavjud', 'when' => function ($model) {
                return !$model->id;
            }],
            [['username', 'full_name', 'password'], 'string', 'min' => 5],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {

        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Login'),
            'full_name' => Yii::t('app', 'Full Name'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'role' => Yii::t('app', 'Role'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return User
     */
    public function register()
    {
        $user = new User();
        $user->username = $this->username;
        $user->role = $this->role;
        $user->status = $this->status;
        $user->full_name = $this->full_name;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generatePasswordResetToken();
        $user->created_at = time();
        $user->updated_at = time();
        if (!$user->save()) {
            var_dump($user->errors);
            die;
        }
        $this->id = $user->id;
        return $user;
    }

    /**
     * @param User $user
     * @return User
     */
    public function update(User $user)
    {
        if($this->password) {
            $user->setPassword($this->password);
        }
        $user->generateAuthKey();
        $user->generatePasswordResetToken();
        $user->updated_at = time();
        if (!$user->save()) {
            ob_start();
            var_dump($user->errors);
            $result = ob_get_clean();
            throw new \DomainException("Foydalanuvchi saqlashda xatolik: " . $result);
        }
        return $user;
    }
}
<?php

namespace backend\models;

use common\models\constants\UserRole;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FileServices;

/**
 * FileServicesSearch represents the model behind the search form of `common\models\FileServices`.
 */
class FileServicesSearch extends FileServices
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['file'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user = \Yii::$app->user->identity;
        $query = FileServices::find();

        if ($user->role == UserRole::ROLE_USER) {
            $query->where(['user_id' => $user->id]);
        } elseif ($user->role == UserRole::ROLE_MODERATOR) {
            $query->innerJoinWith('user')->andWhere(['user.role' => UserRole::ROLE_USER])->select('file_services.*');
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $query->andFilterWhere(['ilike', 'file', $this->file]);
        return $dataProvider;
    }
}

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FileServices */

$this->title = Yii::t('app', 'Create File Services');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'File Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-services-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

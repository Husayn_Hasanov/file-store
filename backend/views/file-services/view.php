<?php

use common\models\FileServices;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FileServices */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'File Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="file-services-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>

        <?php if (Yii::$app->user->identity->is_moderator) { ?>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]); ?>
        <?php } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    /** @var FileServices $model */
                    return $model->user->full_name;
                }
            ],
            [
                'attribute' => 'file',
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var FileServices $model */
                    return Html::a(Yii::t('app', $model->file), ['/uploads/' . $model->original_name], ['target' => '_blank']);
                },
            ]
        ],
    ]) ?>

</div>

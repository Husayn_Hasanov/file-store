<?php

use common\models\FileServices;
use yii\helpers\Html;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FileServicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Fayl Yuklash');
$this->params['breadcrumbs'][] = $this->title;
$user = Yii::$app->user->identity;
?>
<div class="file-services-index">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>

        <?php  if ($user->is_allowed_file_create)
            echo Html::a(Yii::t('app', 'Fayl Yuklash'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    /** @var FileServices $model */
                    return $model->user->full_name;
                }
            ],
            [
                'attribute' => 'file',
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var FileServices $model */
                    return Html::a(Yii::t('app', $model->file), ['/uploads/' . $model->original_name], ['target' => '_blank']);
                }
            ],
            [
                'class' => ActionColumn::className(),
                'template' => $user->is_admin ? "{view} {update} {delete}" : ($user->is_moderator ? "{view} {delete}" : "{view}"),
            ],
        ],
    ]); ?>


</div>

<?php

use common\models\constants\UserRole;
use common\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Foydalanuvchilar');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h2><?= Html::encode($this->title) ?></h2>
    <p>
        <?php if (Yii::$app->user->identity->is_admin) echo Html::a(Yii::t('app', 'Foydalanuvchi qo\'shish'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'full_name',
            [
                'attribute' => 'role',
                'value' => function ($model) {
                    /** @var User $model */
                    return UserRole::getString($model->role);
                },
                'filter' => UserRole::getList()
            ],
            'created_at:datetime',
            'updated_at:datetime',
            [
                'class' => ActionColumn::className(),
            ],
        ],
    ]); ?>


</div>

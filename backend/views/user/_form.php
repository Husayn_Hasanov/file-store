<?php

use common\models\constants\GeneralStatus;
use common\models\constants\UserRole;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'status')->dropDownList(GeneralStatus::getList()) ?>
        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'role')->dropDownList(UserRole::getList()) ?>

            <?= $form->field($model, 'password')->textInput([
                'type' => 'password',
                'maxlength' => true,
                'autocomplete' => 'off',
                'class' => 'form-control',
            ]) ?>

            <?= $form->field($model, 'confirm_password')->textInput([
                'type' => 'password',
                'maxlength' => true,
                'autocomplete' => 'off',
                'class' => 'form-control',
            ]) ?>
        </div>


        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<?php

$js = <<<JS
if($('.change-password').length > 0)
    $('.password-id').attr("disabled", "disabled")

$('.change-password').on("click", function(){
     let value = $('.change-password').is(':checked');
     if(value) 
         $('.password-id').removeAttr("disabled")
     else 
         $('.password-id').attr("disabled", "disabled")
});
JS;
$this->registerJs($js);

?>
